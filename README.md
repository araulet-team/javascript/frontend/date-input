# Date Input
An element providing a starting point for your own reusable Polymer elements.

## Documentation 

http://epok75.github.io/date-input/components/date-input/ 

```
bower install dateinput
```

## Dependencies

Element dependencies are managed via Bower. You can install that via:

```
npm install -g bower
```

Then, go ahead and download the element's dependencies:

```
bower install
```

## Running Element

```
npm install -g polyserve
```

And you can run it via:

```
polyserve
```

Once running, you can preview your element at http://localhost:8080/components/date-input/, where date-input is the name of the directory containing it.



